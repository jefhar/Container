# C11K Container
[![build status](https://gitlab.com/c11k/Container/badges/master/build.svg)](https://gitlab.com/c11k/genesis/commits/master)
[![coverage report](https://gitlab.com/c11k/Container/badges/master/coverage.svg)](https://gitlab.com/c11k/genesis/commits/master)
This is a PSR-11 Standard Container. Container has been forked, modified, extended and hacked from
[https://github.com/sitepoint-editors/Container](https://github.com/sitepoint-editors/Container).

## Table of Contents

- [Installation](#installation)
- [How to Use](#how-to-use)
- [Quick Start](#quick-start)
- [Authors](#authors)
- [License](#license)
- [Support](#support)
- [Contributing](#contributing)

## Installation
```BASH
composer require c11k\container
```
## How to Use

Although it isn't required to do so, a good practice is to split up the configuration for our container. In this example we'll use three files to create our container for the Monolog component.

Another good practice is to use class and interface paths as service names. This provides a stricter naming convention that gives us more information about the services.

In the service definitions file, we define three services. All of the services require constructor injection arguments. Some of these arguments are imported from the container parameters and some are defined directly. The logger service also requires two calls to the `pushHandler` method, each with a different handler service imported.

```PHP
<?php // services.dist.php

// Value objects are used to reference parameters and services in the container
use C11K\Container\Reference\ParameterReference as PR;
use C11K\Container\Reference\ServiceReference as SR;

use Monolog\Logger;
use Monolog\Handler\NativeMailerHandler;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface;

return [
    StreamHandler::class => [
        'class' => StreamHandler::class,
        'arguments' => [
            new PR('logger.file'),
            Logger::DEBUG,
        ],
    ],
    NativeMailHandler::class => [
        'class' => NativeMailerHandler::class,
        'arguments' => [
            new PR('logger.mail.to_address'),
            new PR('logger.mail.subject'),
            new PR('logger.mail.from_address'),
            Logger::ERROR,
        ],
    ],
    LoggerInterface::class => [
        'class' => Logger::class,
        'arguments' => [ 'channel-name' ],
        'calls' => [
            [
                'method' => 'pushHandler',
                'arguments' => [
                    new SR(StreamHandler::class),
                ]
            ],
            [
                'method' => 'pushHandler',
                'arguments' => [
                    new SR(NativeMailHandler::class),
                ]
            ]
        ]
    ]
];
```

The parameters definitions file just returns an array of values. These are defined as an N-dimensional array, but they are accessed through references using the notation: `'logger.file'` or `'logger.mail.to_address'`.

```PHP
<?php // parameters.dist.php

return [
    'logger' => [
        'file' => __DIR__.'/../app.log',
        'mail' => [
            'to_address' => 'webmaster@domain.com',
            'from_address' => 'alerts@domain.com',
            'subject' => 'App Logs',
        ],
    ],
];
```

The container file just extracts the service and parameter definitions and passes them to the `Container` class constructor.


```PHP
<?php // config/container.php

use C11K\Container\Container;

$services   = include __DIR__.'/config/services.php';
$parameters = include __DIR__.'/config/parameters.php';

return new Container($services, $parameters);
```

Now we can obtain the container in our app and use the logger service.

```PHP
<?php // app/file.php

use Psr\Log\LoggerInterface;

require_once __DIR__.'/../vendor/autoload.php';

$container = include __DIR__.'/../config/container.php';

$logger = $container->get(LoggerInterface::class);
$logger->debug('This will be logged to the file');
$logger->error('This will be logged to the file and the email');
```
## Quick Start
Simply return an array of services from `./config/services.php` and return an array of parameters from `./config/parameters.php'`.
```PHP
<?php 
use C11K\Container\Container;
require_once __DIR__ . '/../vendor/bin/autoload.php';
$contatiner = Container::createFromConfig();
```
`$container->get()` will return the same object each time. If you need a freshly instantiated $service,
call `$container->factory()`.
## Authors

- [Andrew Carter](https://twitter.com/AndrewCarterUK)
- Fork by [Jeff Harris](https://gitlab.com/c11k)

## License

The MIT License (MIT). Please see [LICENSE](LICENSE) for more information.
## Support

Please [open an issue](https://gitlab.com/c11k/Container/issues/new) for support.

## Contributing

Please contribute using [Gitlab Flow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html). Create a branch, add commits, and [open a pull request](https://gitlab.com/c11k/Container/merge_requests/new).
