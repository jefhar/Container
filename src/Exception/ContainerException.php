<?php
/**
 * PHP Version 7.1
 *
 * @package c11k/container
 * throws Psr\Container\ContainerExceptionInterface
 */

declare(strict_types=1);

namespace C11K\Container\Exception;

use Psr\Container\ContainerExceptionInterface;

class ContainerException extends \Exception implements ContainerExceptionInterface
{

}
