<?php
/**
 * PHP Version 7.1
 *
 * @package c11k/container
 */
namespace C11K\Container;

use C11K\Container\Exception\ParameterNotFound;

interface ContainerInterface extends \Psr\Container\ContainerInterface
{
    /**
     * @param string $parameter
     * @return array|mixed
     * @throws ParameterNotFound
     */
    public function getParameter(string $parameter);
    public function hasParameter(string $parameter);
}
