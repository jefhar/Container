<?php
/**
 * PHP Version 7.1
 *
 * @package c11k/container
 */

declare(strict_types=1);

namespace C11K\Container;

use C11K\Container\Exception\ContainerException;
use C11K\Container\Exception\ParameterNotFound;
use C11K\Container\Exception\ServiceNotFound;
use C11K\Container\Reference\ParameterReference;
use C11K\Container\Reference\ServiceReference;

class Container implements ContainerInterface
{
    private $services;
    private $parameters;
    private $serviceStore;

    /**
     * Container constructor.
     *
     * @param array $services
     * @param array $parameters
     */
    public function __construct(array $services = [], array $parameters = [])
    {
        $this->services = $services;
        $this->parameters = $parameters;
        $this->serviceStore = [];
    }

    /**
     * @param mixed $service
     * @return mixed
     * @throws \C11K\Container\Exception\ContainerException
     * @throws \C11K\Container\Exception\ServiceNotFound
     */
    public function get($service)
    {
        if (!$this->has($service)) {
            throw new ServiceNotFound('Service not found: ' . $service);
        }

        if (!isset($this->serviceStore[$service])) {
            $this->serviceStore[$service] = $this->createService($service, true);
        }

        return $this->serviceStore[$service];
    }

    /**
     * Make a brand new $service
     *
     * @param mixed $service
     * @return object
     * @throws \C11K\Container\Exception\ContainerException
     * @throws \C11K\Container\Exception\ServiceNotFound
     */
    public function factory($service)
    {
        return $this->createService($service, false);
    }

    /**
     * @param mixed $service
     * @return bool
     */
    public function has($service): bool
    {
        return isset($this->services[$service]);
    }

    /**
     * @param string $name
     * @param bool $lock
     * @return object
     * @throws \C11K\Container\Exception\ContainerException
     * @throws \C11K\Container\Exception\ServiceNotFound
     */
    private function createService(string $name, bool $lock = true)
    {
        $entry = &$this->services[$name];

        if (!is_array($entry) || (!isset($entry['class']) && !isset($entry['function']))) {
            throw new ContainerException($name . ' service entry must be an array containing a \'class\' or a \'function\' key');
        }

        if (isset($entry['function'])) {
            if (is_callable($entry['function'])) {
                $arguments = isset($entry['arguments']) ? $this->resolveArguments($entry['arguments']) : [];

                return call_user_func_array($entry['function'], $arguments);
            } else {
                throw new ContainerException($name . ' service entry \'function\' key must be callable');
            }
        }

        if (!class_exists($entry['class'])) {
            throw new ContainerException($name . ' service class does not exist: ' . $entry['class']);
        }

        if (isset($entry['lock']) && $lock) {
            throw new ContainerException($name . ' service contains a circular reference.');
        }

        // $entry['lock'] might already be true and we're getting a factory
        if ($lock) {
            $entry['lock'] = true;
        }

        $arguments = isset($entry['arguments']) ? $this->resolveArguments($entry['arguments']) : [];
        $reflector = new \ReflectionClass($entry['class']);
        $service = $reflector->newInstanceArgs($arguments);

        if (isset($entry['calls'])) {
            $this->initializeService($service, $name, $entry['calls']);
        }

        return $service;
    }

    /**
     * @param array $argumentDefinitions
     * @return array
     * @throws ContainerException
     * @throws ParameterNotFound
     * @throws ServiceNotFound
     */
    private
    function resolveArguments(
        array $argumentDefinitions
    ) {
        $arguments = [];
        foreach ($argumentDefinitions as $argumentDefinition) {
            if ($argumentDefinition instanceof ServiceReference) {
                $argumentServiceName = $argumentDefinition->getName();
                $arguments[] = $this->get($argumentServiceName);
            } elseif ($argumentDefinition instanceof ParameterReference) {
                $argumentParameterName = $argumentDefinition->getName();
                $arguments[] = $this->getParameter($argumentParameterName);
            } else {
                $arguments[] = $argumentDefinition;
            }
        }

        return $arguments;
    }

    /**
     * @param string $parameter
     * @return array|mixed
     * @throws ParameterNotFound
     */
    public
    function getParameter(
        string $parameter
    ) {
        $tokens = explode('.', $parameter);
        $context = $this->parameters;

        while (null !== ($token = array_shift($tokens))) {
            if (!isset($context[$token])) {
                throw new ParameterNotFound('Parameter not found: ' . $parameter);
            }

            $context = $context[$token];
        }

        return $context;
    }

    /**
     * @param mixed $service
     * @param string $name
     * @param array $callDefinitions
     * @throws \C11K\Container\Exception\ContainerException
     * @throws \C11K\Container\Exception\ServiceNotFound
     */
    private
    function initializeService(
        $service,
        string $name,
        array $callDefinitions
    ) {
        foreach ($callDefinitions as $callDefinition) {
            if (!is_array($callDefinition) || !isset($callDefinition['method'])) {
                throw new ContainerException($name . ' service calls must be arrays containing a \'method\' key.');
            } elseif (!is_callable([$service, $callDefinition['method']])) {
                throw new ContainerException($name . ' service asks for call to uncallable method: ' .
                    $callDefinition['method']);
            }

            $arguments = isset($callDefinition['arguments']) ? $this->resolveArguments($callDefinition['arguments']) : [];

            call_user_func_array([$service, $callDefinition['method']], $arguments);
        }
    }

    /**
     * @param string $parameter
     * @return bool
     */
    public
    function hasParameter(
        string $parameter
    ): bool {
        try {
            $this->getParameter($parameter);
        } catch (ParameterNotFound $exception) {
            return false;
        }

        return true;
    }

    /**
     * @return Container
     * @throws ContainerException
     */
    public
    static function createFromConfig(): self
    {
        $servicesFile = __DIR__ . '/../config/services.php';
        $parametersFile = __DIR__ . '/../config/parameters.php';

        $services = self::fetchArrayFromFile($servicesFile, 'Services file missing from config directory.');
        $parameters = self::fetchArrayFromFile($parametersFile, 'Parameters file missing from config directory.');

        return new self($services, $parameters);
    }

    /**
     * @param string $filename
     * @param string $exceptionMessage
     * @return array
     * @throws \C11K\Container\Exception\ContainerException
     */
    private
    static function fetchArrayFromFile(
        string $filename,
        string $exceptionMessage
    ): array {
        if (!file_exists($filename)) {
            throw new ContainerException($exceptionMessage);
        }
        $process = include $filename;
        if (gettype($process) !== 'array') {
            $process = [];
        }

        return $process;
    }
}
