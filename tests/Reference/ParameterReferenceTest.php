<?php

namespace C11K\Container\Tests\Reference;

use C11K\Container\Reference\ParameterReference;
use PHPUnit\Framework\TestCase;

class ParameterReferenceTest extends TestCase
{
    public function testParameterReference()
    {
        $reference = new ParameterReference('foo.bar');
        $this->assertEquals('foo.bar', $reference->getName());
    }
}
