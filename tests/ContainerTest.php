<?php

namespace C11K\Container\Tests;

use C11K\Container\Container;
use C11K\Container\Reference\ParameterReference;
use C11K\Container\Reference\ServiceReference;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class ContainerTest extends TestCase
{
    public function testParameters()
    {
        $parameters = [
            'hello' => 'world',
            'first' => [
                'second' => 'foo',
                'third' => [
                    'fourth' => 'bar',
                ],
            ],
        ];

        $container = new Container([], $parameters);

        $this->assertEquals('world', $container->getParameter('hello'), "Parameter 'hello'.");
        $this->assertEquals('foo', $container->getParameter('first.second'), "Parameter 'first.second'.");
        $this->assertEquals('bar', $container->getParameter('first.third.fourth'), "Parameter: 'first.third.fourth'.");
    }

    public function testContainer()
    {
        // Service definitions
        $services = [
            'service' => [
                'class' => MockService::class,
                'arguments' => [
                    new ServiceReference('dependency'),
                    'foo',
                ],
                'calls' => [
                    [
                        'method' => 'setProperty',
                        'arguments' => [
                            new ParameterReference('group.param'),
                        ],
                    ],
                ],
            ],
            'dependency' => [
                'class' => MockDependency::class,
                'arguments' => [
                    new ParameterReference('group.param'),
                ],
            ],
        ];
        // Parameter definitions
        $parameters = [
            'group' => [
                'param' => 'bar',
            ],
        ];
        // Create container
        $container = new Container($services, $parameters);
        $this->assertInstanceOf(ContainerInterface::class, $container, 'Container instance of Container.');
        // Check retrieval of service
        $service = $container->get('service');
        $this->assertInstanceOf(MockService::class, $service, "'service' instance correct.");
        // Check retrieval of dependency
        $dependency = $container->get('dependency');
        $this->assertInstanceOf(MockDependency::class, $dependency, "'dependency' instance correct.");
        // Check that the dependency has been reused
        $this->assertSame($dependency, $service->getDependency());
        // Check the retrieval of container parameters
        $this->assertTrue($container->hasParameter('group.param'), "Parameter 'group.param'.");
        $this->assertFalse($container->hasParameter('foo.bar'), "Parameter 'foo.bar'.");
        // Check the parameters have been loaded correctly
        $this->assertEquals('foo', $service->getParameter(), "Parameter 'foo' belongs to \$service.");
        $this->assertEquals('bar', $dependency->getParameter(), "Parameter 'bar' belongs to \$dependency.");
        // Check the service calls have initialized
        $this->assertEquals('bar', $service->getProperty(), "Parameter 'bar' belongs to \$service.");

        // Check if the container returns the same instance
        $changedService = $container->get('service');
        $changedService->someField = true;
        $this->assertTrue($changedService === $container->get('service'), "Returned class is attached.");

        // Check retrieval of factory service
        $this->assertTrue($container->get('service') !== $container->factory('service'));
        $this->assertTrue($container->factory('service') !== $container->factory('service'));
    }

    public function testCreateFromConfig()
    {
        $servicesLocation = __DIR__ . '/../config/services.php';
        $parametersLocation = __DIR__ . '/../config/parameters.php';
        touch($servicesLocation);
        touch($parametersLocation);
        $container = Container::createFromConfig();
        $this->assertInstanceOf(Container::class, $container, 'Container from config is correct instance.');
    }
    // ERROR TESTING

    /**
     * @expectedException \Psr\Container\NotFoundExceptionInterface
     */
    public function testServiceNotFound()
    {
        $container = new Container();
        $container->get('foo');
    }

    /**
     * @expectedException \C11k\Container\Exception\ParameterNotFound
     */
    public function testParameterNotFound()
    {
        $container = new Container();
        $container->getParameter('foo');
    }

    /**
     * @expectedException        \Psr\Container\ContainerExceptionInterface
     * @expectedExceptionMessage must be an array containing a 'class' or a 'function'
     */
    public function testBadServiceEntry()
    {
        $container = new Container(['foo' => 'bar']);
        $container->get('foo');
    }

    /**
     * @expectedException        \Psr\Container\ContainerExceptionInterface
     * @expectedExceptionMessage class does not exist
     */
    public function testInvalidClassPath()
    {
        $container = new Container(['foo' => ['class' => 'LALALALALALA']]);
        $container->get('foo');
    }

    /**
     * @expectedException        \Psr\Container\ContainerExceptionInterface
     * @expectedExceptionMessage circular reference
     */
    public function testCircularReference()
    {
        $container = new Container([
            'foo' => [
                'class' => MockService::class,
                'arguments' => [
                    new ServiceReference('bar'),
                ],
            ],
            'bar' => [
                'class' => MockService::class,
                'arguments' => [
                    new ServiceReference('foo'),
                ],
            ],
        ]);
        $container->get('foo');
    }

    /**
     * @expectedException        \Psr\Container\ContainerExceptionInterface
     * @expectedExceptionMessage service calls must be arrays containing a 'method' key
     */
    public function testNoMethod()
    {
        $container = new Container([
            'foo' => [
                'class' => MockDependency::class,
                'arguments' => [
                    'foo',
                ],
                'calls' => [
                    ['foo'],
                ],
            ],
        ]);
        $container->get('foo');
    }

    /**
     * @expectedException        \Psr\Container\ContainerExceptionInterface
     * @expectedExceptionMessage call to uncallable method
     */
    public function testUncallableMethod()
    {
        $container = new Container([
            'foo' => [
                'class' => MockDependency::class,
                'arguments' => [
                    'foo',
                ],
                'calls' => [
                    ['method' => 'LALALALALA'],
                ],
            ],
        ]);
        $container->get('foo');
    }

    /**
     * @expectedException \Psr\Container\ContainerExceptionInterface
     */
    public function testCreateFromConfigThrowsExceptionMissingServices()
    {
        $servicesLocation = __DIR__ . '/../config/services.php';
        $parametersLocation = __DIR__ . '/../config/parameters.php';
        touch($servicesLocation);
        unlink($servicesLocation);
        $container = Container::createFromConfig();
    }

    /**
     * @expectedException \C11k\Container\Exception\ContainerException
     */
    public function testCreateFromConfigThrowsExceptionMissingParameters()
    {
        $servicesLocation = __DIR__ . '/../config/services.php';
        $parametersLocation = __DIR__ . '/../config/parameters.php';
        touch($servicesLocation);
        touch($parametersLocation);
        unlink($parametersLocation);
        $container = Container::createFromConfig();
    }

    public function testFunction()
    {
        $container = new Container([
            'trueFunction' => [
                'function' => function () {
                    return true;
                },
            ],
            'trueFunctionWithArg' => [
                'function' => function ($arg) {
                    return $arg;
                },
                'arguments' => [
                    true,
                ],
            ],
        ]);
        $trueFunction = $container->get('trueFunction');
        $this->assertTrue($trueFunction, 'Function is true.');

        $trueFunctionArgs = $container->get('trueFunctionWithArg');
        $this->assertTrue($trueFunctionArgs, 'Function with Arguments is true.');
    }

    /**
     * @expectedException        \Psr\Container\ContainerExceptionInterface
     * @expectedExceptionMessage 'function' key must be callable
     */
    public function testFunctionNotCallable()
    {
        $container = new Container([
            'badFunction' => [
                'function' => [],
            ],
        ]);
        $trueFunction = $container->get('badFunction');
    }

    /**
     * @expectedException        \Psr\Container\ContainerExceptionInterface
     * @expectedExceptionMessage must be an array containing a 'class' or a 'function' key
     */
    public function testMissingClassAndFunction()
    {
        $container = new Container([
            'EmptyService' => [],
        ]);
        $trueFunction = $container->get('EmptyService');
    }

    public function testFunctionCanReturnInstance()
    {
        // Service definitions
        $services = [
            'functionService' => [
                'function' => function ($arg) {
                    return new MockDependency($arg);
                },
                'arguments' => [
                    'foo',
                ],
            ],
            'parameterInArgument' => [
                'function' => function ($arg) {
                    return new MockDependency($arg);
                },
                'arguments' => [
                    new ParameterReference('group.param'),
                ],
            ],
        ];
        // Parameter definitions
        $parameters = [
            'group' => [
                'param' => 'bar',
            ],
        ];
        // Create container
        $container = new Container($services, $parameters);
        $instanceFunction = $container->get('functionService');
        $this->assertInstanceOf(MockDependency::class, $instanceFunction,
            'Function returning new ServiceReference returns correct instance.');
        $parameterizedFunction = $container->get('parameterInArgument');
        $this->assertEquals('bar', $parameterizedFunction->getParameter());
    }
}

// Mock classes for testing
class MockService
{
    private $dependency;
    private $parameter;
    private $property;

    public function __construct(MockDependency $dependency, $parameter)
    {
        $this->dependency = $dependency;
        $this->parameter = $parameter;
    }

    public function getDependency()
    {
        return $this->dependency;
    }

    public function getParameter()
    {
        return $this->parameter;
    }

    public function getProperty()
    {
        return $this->property;
    }

    public function setProperty($value)
    {
        $this->property = $value;
    }
}

class MockDependency
{
    private $parameter;

    public function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    public function getParameter()
    {
        return $this->parameter;
    }

}
