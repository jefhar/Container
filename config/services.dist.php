<?php // config/services.dist.php

// Value objects are used to reference parameters and services in the container
use C11K\Container\Reference\ParameterReference as PR;
use C11K\Container\Reference\ServiceReference as SR;
use Nette\Utils\ArrayHash;
use TheSeer\Tokenizer\NamespaceUri;
use TheSeer\Tokenizer\XMLSerializer;


return [
    'trueFunction' => [
        'function' => function () {
            return true;
        },
    ],
    'trueFunctionWithArg' => [
        'function' => function($arg) {
            return $arg;
        },
        'arguments' =>[
            true,
        ]
    ],
    XMLSerializer::class => [
        'class' => XMLSerializer::class,
        'arguments' => [
            new SR(NamespaceUri::class),
        ],
    ],
    NamespaceUri::class => [
        'class' => NamespaceUri::class,
        'arguments' => [
            new PR('site.address'),
        ],
    ],
    ArrayHash::class => [
        'class' => ArrayHash::class,
    ],
];
